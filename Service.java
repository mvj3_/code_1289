public class PhoneListenerService extends Service {
@Override
public void onCreate() {
        TelephonyManager manager = 
                  (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		//监听电话的状态
	manager.listen(new MyListener(), PhoneStateListener.LISTEN_CALL_STATE);
}

private final class MyListener extends PhoneStateListener {
	private String num;
	private MediaRecorder recorder;
	public void onCallStateChanged(int state, String incomingNumber) {
		switch (state) {
			case TelephonyManager.CALL_STATE_RINGING:  /* 电话进来时 */
				num = incomingNumber;
				break;
			case TelephonyManager.CALL_STATE_OFFHOOK: /* 接起电话时 */
				try {
					File file = new File(Environment.getExternalStorageDirectory(), num + "_" + System.currentTimeMillis() + ".3gp");
					recorder = new MediaRecorder();
					recorder.setAudioSource(AudioSource.MIC);//声音采集来源(话筒)
					recorder.setOutputFormat(OutputFormat.THREE_GPP);//输出的格式
					recorder.setAudioEncoder(AudioEncoder.AMR_NB);//音频编码方式
					recorder.setOutputFile(file.getAbsolutePath());//输出方向
					recorder.prepare();
					recorder.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case TelephonyManager.CALL_STATE_IDLE:  /* 无任何状态时 */
				if (recorder != null) {
					recorder.stop();
					recorder.release();
				}
				break;
		}
	}
}
}